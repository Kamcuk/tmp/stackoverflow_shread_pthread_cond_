#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <memory>
#include <signal.h>

#include "Binary_Semaphore.h"

static const std::string BSEM = R"(/BSEM)";
std::unique_ptr<Binary_Semaphore> binary_sem{new Binary_Semaphore(BSEM, true, true)};
void clean(int) { binary_sem.reset();  exit(0); }
int main() {
	signal(SIGTERM, clean);
    while (true) {
        binary_sem->post();
        std::cout << "signal posted" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(100LL));
    }
}
