.SECONDARY:
CXXFLAGS = -Wall -Wextra -std=c++11 -pthread

all: test

test: _build/cnt.txt
	@echo "test: signal received $$(cat $^) times"
	@if [ "$$(cat $^)" -lt 25 ]; then \
		echo "test: TEST FAILED: signal received less then 25 times"; \
		false; \
	fi
	@echo "test:SUCCESS"

_build/cnt.txt: _build/output.txt
	grep -c 'signal received' _build/output.txt > _build/cnt.txt

.PHONY: _build/output.txt
_build/output.txt: _build/first_process _build/second_process
	( \
		set -x ; \
		timeout 4 strace -v -s 200 -o _build/$(NAME)first_process.strace.txt _build/first_process & \
		sleep 0.2 ; \
		timeout 0.3 strace -v -s 200 -o _build/$(NAME)second_process_1.strace.txt _build/second_process ; \
		sleep 0.5 ; \
		timeout 2.5 strace -v -s 200 -o _build/$(NAME)second_process_2.strace.txt _build/second_process ; \
		wait; \
	) 2>&1 | tee _build/output.txt || true

_build/first_process: Binary_Semaphore.o First_Process.o
	@mkdir -p $(@D)
	g++ $(CXXFLAGS) $^ -o $@ -lrt

_build/second_process: Binary_Semaphore.o Second_Process.o
	@mkdir -p $(@D)
	g++ $(CXXFLAGS) $^ -o $@ -lrt

clean:
	rm -rf *.o _build
