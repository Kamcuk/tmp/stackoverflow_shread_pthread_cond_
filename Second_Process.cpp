#include <iostream>
#include <string>
#include <memory>
#include "Binary_Semaphore.h"
#include <signal.h>

static const std::string BSEM = R"(/BSEM)";
std::unique_ptr<Binary_Semaphore> binary_sem{new Binary_Semaphore(BSEM, true, false)};
void clean(int) { binary_sem.reset(); exit(0); }
int main() {
	signal(SIGTERM, clean);
    while (true) {
        binary_sem->wait();
        std::cout << "signal received" << std::endl;
    }
}
