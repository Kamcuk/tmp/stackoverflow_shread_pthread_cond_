#ifndef BINARY_SEMAPHORE_H
#define BINARY_SEMAPHORE_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <unistd.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
       #include <sys/stat.h>
       #include <fcntl.h>

struct binary_semaphore_attr {
	pthread_mutexattr_t mutex_attr;
    pthread_mutex_t mutex;
	pthread_condattr_t cvar_attr;
    pthread_cond_t cvar;
    volatile bool flag;
};

class Binary_Semaphore {

    struct binary_semaphore_attr *bin_sem_attr;
    const std::string bin_sem_attr_shm_ID;
    int bin_sem_ID { -1 };

    const bool is_process_shared;
    const bool is_to_be_created;

public:
    Binary_Semaphore(const std::string& bin_sem_ID, const bool is_process_shared, const bool is_to_be_created);
    ~Binary_Semaphore();
    
    void post();
    void wait();

    template<typename T>
    static void create_shared_memory(T **shm, const std::string& shm_ID, int& ID, const bool is_to_be_created, const int o_flags, const int mode) {
        if ((ID = shm_open(shm_ID.c_str(), o_flags, mode)) == -1) {
            std::cerr << "shm_open failed with " << shm_ID << "\n";
            exit(EXIT_FAILURE);
        }

        if (is_to_be_created) {
            if (ftruncate(ID, sizeof(T)) == -1) {
                std::cerr << "ftruncate failed with " << shm_ID << "\n";
                exit(EXIT_FAILURE);
            }
        }

        if ((*shm = reinterpret_cast<T*>(mmap(nullptr, sizeof(T), PROT_READ | PROT_WRITE, MAP_SHARED, ID, 0))) == MAP_FAILED) {
            std::cerr << "mmap failed with " << shm_ID << "\n";
            exit(EXIT_FAILURE);
        }
    }
};

#endif
