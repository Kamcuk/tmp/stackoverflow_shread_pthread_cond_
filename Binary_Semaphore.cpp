#include "Binary_Semaphore.h"
#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>

#define dbg(fmt, ...)  \
	fprintf(stderr, "%s: %s:%d: " fmt "\n", \
		program_invocation_short_name, \
		__PRETTY_FUNCTION__, \
		__LINE__, ##__VA_ARGS__)

#define CHECK(x)  ({ \
		dbg("[[ %s", #x); \
		const int _r = x; \
		dbg("]] %s = %d %s", #x, _r, strerror(_r)); \
		assert(_r == 0); \
		_r; \
		})

Binary_Semaphore::Binary_Semaphore(const std::string& bin_sem_ID, const bool is_process_shared, const bool is_to_be_created) : bin_sem_attr_shm_ID(bin_sem_ID), is_process_shared(is_process_shared), is_to_be_created(is_to_be_created) {
    /* set binary semaphore attribute */
    if (is_to_be_created) {
        if (is_process_shared) {
            create_shared_memory(&bin_sem_attr, bin_sem_attr_shm_ID, this->bin_sem_ID, is_to_be_created,
					O_EXCL | O_CREAT | O_RDWR | O_TRUNC, S_IRWXU | S_IRWXG);

            /* set mutex shared between processes */
            auto& mutex_attr = bin_sem_attr->mutex_attr;
            CHECK(pthread_mutexattr_init(&mutex_attr));
            CHECK(pthread_mutexattr_setpshared(&mutex_attr, PTHREAD_PROCESS_SHARED));
            CHECK(pthread_mutexattr_setrobust(&mutex_attr, PTHREAD_MUTEX_ROBUST));
			CHECK(pthread_mutexattr_settype(&mutex_attr, PTHREAD_MUTEX_ERRORCHECK));
            CHECK(pthread_mutex_init(&bin_sem_attr->mutex, &mutex_attr));
            //CHECK(pthread_mutexattr_destroy(&mutex_attr));

            /* set cvar shared between processes */
            auto& cvar_attr = bin_sem_attr->cvar_attr;
            CHECK(pthread_condattr_init(&cvar_attr));
            CHECK(pthread_condattr_setpshared(&cvar_attr, PTHREAD_PROCESS_SHARED));
            CHECK(pthread_cond_init(&bin_sem_attr->cvar, &cvar_attr));
            //CHECK(pthread_condattr_destroy(&cvar_attr));
			dbg("created");
        } else {
            bin_sem_attr = new binary_semaphore_attr();
		}
    } else {
        if (is_process_shared)
            create_shared_memory(&bin_sem_attr, bin_sem_attr_shm_ID, this->bin_sem_ID, is_to_be_created, O_RDWR, S_IRUSR | S_IRGRP | S_IWUSR | S_IWGRP);
    }
}

Binary_Semaphore::~Binary_Semaphore() {
    if (is_to_be_created) {
		dbg("cleanup");
        CHECK(pthread_mutex_destroy(&bin_sem_attr->mutex));
        // CHECK(pthread_cond_destroy(&bin_sem_attr->cvar)); // blocks ??

        CHECK(munmap(bin_sem_attr, sizeof(binary_semaphore_attr)));
        CHECK(close(this->bin_sem_ID));
        CHECK(shm_unlink(bin_sem_attr_shm_ID.c_str()));
    } else {
		dbg("no cleanup");
	}
}

void Binary_Semaphore::post() {
	auto bin_sem_mutex = &bin_sem_attr->mutex;
	volatile bool *bin_sem_data = &bin_sem_attr->flag;
	auto bin_sem_cvar = &bin_sem_attr->cvar;
	int r;
	CHECK((r = pthread_mutex_lock(bin_sem_mutex)));
    if (r == EOWNERDEAD) {
		dbg("Consistent");
        CHECK(pthread_mutex_consistent(bin_sem_mutex));
	}
	dbg("HERE: %d", *bin_sem_data);
    *bin_sem_data = true;
    CHECK(pthread_mutex_unlock(bin_sem_mutex));
    CHECK(pthread_cond_signal(bin_sem_cvar));
}

void Binary_Semaphore::wait() {
	auto bin_sem_mutex = &bin_sem_attr->mutex;
	volatile bool *bin_sem_data = &bin_sem_attr->flag;
	auto bin_sem_cvar = &bin_sem_attr->cvar;
	int r;
	CHECK((r = pthread_mutex_lock(bin_sem_mutex)));
    if (r == EOWNERDEAD) {
		dbg("Consistent");
        CHECK(pthread_mutex_consistent(bin_sem_mutex));
	}
    while (!(*bin_sem_data)) {
		dbg("HERE: %d", *bin_sem_data);
        CHECK(pthread_cond_wait(bin_sem_cvar, bin_sem_mutex));
	}
    *bin_sem_data = false;
    CHECK(pthread_mutex_unlock(bin_sem_mutex));
}



